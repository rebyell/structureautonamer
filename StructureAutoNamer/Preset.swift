import Cocoa

class Preset {
  var h_1: Bool = false
  var h_2: Bool = false
  var h_3: Bool = false
  var h_4: Bool = false
  var h_5: Bool = false
  var h_6: Bool = false
  var h_7: Bool = false
  var h_b2: Bool = false
  var h_b3: Bool = false
  var h_b5: Bool = false
  var h_b6: Bool = false
  var h_b7: Bool = false
  var name: String = "Default Preset"
  var root: Int = 0;
  
  func setAll(state: Bool) {
    self.h_1 = state
    self.h_2 = state
    self.h_3 = state
    self.h_4 = state
    self.h_5 = state
    self.h_6 = state
    self.h_7 = state
    self.h_b2 = state
    self.h_b3 = state
    self.h_b5 = state
    self.h_b6 = state
    self.h_b7 = state
  }
  
  func randomizeSelection() {
    print("randomizing")
    self.h_1 = Bool.random()
    self.h_2 = Bool.random()
    self.h_3 = Bool.random()
    self.h_4 = Bool.random()
    self.h_5 = Bool.random()
    self.h_6 = Bool.random()
    self.h_7 = Bool.random()
    self.h_b2 = Bool.random()
    self.h_b3 = Bool.random()
    self.h_b5 = Bool.random()
    self.h_b6 = Bool.random()
    self.h_b7 = Bool.random()
  }

  func description() -> String {
    var ret: String = ""
    
    ret =  "name: " +  name + "\n"
    ret += "root:" + root.description + "\n"
    ret += "h1  : " + h_1.description + "\n"
    ret += "hb2 : " + h_b2.description + "\n"
    ret += "h2  : " + h_2.description + "\n"
    ret += "hb3 : " + h_b3.description + "\n"
    ret += "h3  : " + h_3.description + "\n"
    ret += "h4  : " + h_4.description + "\n"
    ret += "hb5 : " + h_b5.description + "\n"
    ret += "h5  : " + h_5.description + "\n"
    ret += "hb6 : " + h_b6.description + "\n"
    ret += "h6  : " + h_6.description + "\n"
    ret += "hb7 : " + h_b7.description + "\n"
    ret += "h7  : " + h_7.description + "\n"
    
    return ret
  }
}
