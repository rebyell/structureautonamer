//
//  MainViewController.swift
//  StructureAutoNamer
//
//  Created by Reto Byell on 25.04.20.
//  Copyright © 2020 Reto Byell. All rights reserved.
//

import Foundation
import Cocoa

class MainViewController : NSViewController {
  var p: Preset = Preset()
  var autoname: String = String()
  
  @IBOutlet weak var cbH1: NSButton!
  @IBOutlet weak var cbHb2: NSButton!
  @IBOutlet weak var cbH2: NSButton!
  @IBOutlet weak var cbHb3: NSButton!
  @IBOutlet weak var cbH3: NSButton!
  @IBOutlet weak var cbH4: NSButton!
  @IBOutlet weak var cbHb5: NSButton!
  @IBOutlet weak var cbH5: NSButton!
  @IBOutlet weak var cbHb6: NSButton!
  @IBOutlet weak var cbH6: NSButton!
  @IBOutlet weak var cbHb7: NSButton!
  @IBOutlet weak var cbH7: NSButton!
  
  @IBOutlet var textAutoname: NSTextField!
  
  
  
  @IBAction func cbClicked(_ sender: Any) {
    print("clicked")
    checkBoxesToPreset()
    updateDisplay()
  }
  
  func updateDisplay() {
    print(p.description())
    presetToCheckBoxes()
    autoname = getAutoname()
    print("Autoname:", autoname)
    textAutoname.stringValue = autoname
  }
  
  
  @IBAction func selectNotesAll(_ sender: Any) {
    print("selectAll")
    p.setAll(state: true)
    presetToCheckBoxes()
    updateDisplay()
  }
  
  @IBAction func selectNotesNone(_ sender: Any) {
    print("selectNone")
    p.setAll(state: false)
    presetToCheckBoxes()
    updateDisplay()
  }
  
  @IBAction func randomize(_ sender:Any) {
    p.randomizeSelection()
    print(p.description())
    presetToCheckBoxes()
    updateDisplay()
  }
  
  func checkBoxesToPreset() {
    p.h_1 = NSNumber(value: cbH1.state.rawValue).boolValue
    p.h_b2 = NSNumber(value: cbHb2.state.rawValue).boolValue
    p.h_2 = NSNumber(value: cbH2.state.rawValue).boolValue
    p.h_b3 = NSNumber(value: cbHb3.state.rawValue).boolValue
    p.h_3 = NSNumber(value: cbH3.state.rawValue).boolValue
    p.h_4 = NSNumber(value: cbH4.state.rawValue).boolValue
    p.h_b5 = NSNumber(value: cbHb5.state.rawValue).boolValue
    p.h_5 = NSNumber(value: cbH5.state.rawValue).boolValue
    p.h_b6 = NSNumber(value: cbHb6.state.rawValue).boolValue
    p.h_6 = NSNumber(value: cbH6.state.rawValue).boolValue
    p.h_b7 = NSNumber(value: cbHb7.state.rawValue).boolValue
    p.h_7 = NSNumber(value: cbH7.state.rawValue).boolValue
  }
  
  func presetToCheckBoxes() {
    if p.h_1 != NSNumber(value: cbH1.state.rawValue).boolValue { cbH1.setNextState() }
    if p.h_b2 != NSNumber(value: cbHb2.state.rawValue).boolValue { cbHb2.setNextState() }
    if p.h_2 != NSNumber(value: cbH2.state.rawValue).boolValue { cbH2.setNextState() }
    if p.h_b3 != NSNumber(value: cbHb3.state.rawValue).boolValue { cbHb3.setNextState() }
    if p.h_3 != NSNumber(value: cbH3.state.rawValue).boolValue { cbH3.setNextState() }
    if p.h_4 != NSNumber(value: cbH4.state.rawValue).boolValue { cbH4.setNextState() }
    if p.h_b5 != NSNumber(value: cbHb5.state.rawValue).boolValue { cbHb5.setNextState() }
    if p.h_5 != NSNumber(value: cbH5.state.rawValue).boolValue { cbH5.setNextState() }
    if p.h_b6 != NSNumber(value: cbHb6.state.rawValue).boolValue { cbHb6.setNextState() }
    if p.h_6 != NSNumber(value: cbH6.state.rawValue).boolValue { cbH6.setNextState() }
    if p.h_b7 != NSNumber(value: cbHb7.state.rawValue).boolValue { cbHb7.setNextState() }
    if p.h_7 != NSNumber(value: cbH7.state.rawValue).boolValue { cbH7.setNextState() }
  }
  
  
  
  func getAutoname() -> String {
    var ret:String = "Undefined"
    var root: String = ""
    var qual: String = " undefined"
    var terz: String = "" // "dur", "moll", "sus4", "sus2" <- order of prio
    var sept: String = "" // "b7", "7", "6" <- order of prio
    
    if(p.root == 0) {
      root = "C"
    }
    
    terz = terzErmitteln()
    sept = septErmitteln()
    print("terz: ", terz)
    print("sept: ", sept)
    
    // Dur terz
    if(terz == "dur") {
      // dominant
      if(sept == "b7") {
        qual = "7"
        
        if(p.h_7) {
          qual += " j7"
        }
        
        if(p.h_b5){
          var b5:String = " b5"
          if(p.h_5) {
            b5 = " #11"
          }
          qual += b5
        }
        
        if(p.h_2) {
          qual += " 9"
        }
        if(p.h_b2) {
          qual += " b9"
        }
        if(p.h_b3) {
          qual += " #9"
        }
        
        if(p.h_4) {
          qual += " 11"
        }
        
        if(p.h_6) {
          qual += " 13"
        }
        if(p.h_b6) {
          qual += " b13"
        }
      }
      // tonika
      if(sept == "7") {
        qual = "j7"
               
        if(p.h_b5){
          var b5:String = " b5"
          if(p.h_5) {
            b5 = " #11"
          }
          qual += b5
        }
        
        if(p.h_2) {
          qual += " 9"
        }
        if(p.h_b2) {
          qual += " b9"
        }
        if(p.h_b3) {
          qual += " #9"
        }
        
        if(p.h_4) {
          qual += " 11"
        }
        
        if(p.h_6) {
          qual += " 13"
        }
        if(p.h_b6) {
          qual += " b13"
        }
      }
    }
    
    // Moll terz
    if(terz == "moll") {
      // Moll sept
      if(sept == "b7") {
        qual = "m7"
        if(p.h_7) {
          qual = "mj7"
        }
        
        if(p.h_b5){
          var b5:String = " b5"
          if(p.h_5) {
            b5 = " #11"
          }
          qual += b5
        }
        
        if(p.h_2) {
          qual += " 9"
        }
        if(p.h_b2) {
          qual += " b9"
        }
        
        if(p.h_4) {
          qual += " 11"
        }
        
        if(p.h_6) {
          qual += " 13"
        }
        if(p.h_b6) {
          qual += " b13"
        }
      }
    }
    
    
    if(terz == "sus4") {
      
    }
    
    
    if(terz == "sus2") {
      
    }
    
    ret = root + qual
    return ret
    
  }
  
  
  func terzErmitteln() -> String {
    var ret: String = ""
    if(p.h_2) {
      ret = "sus2"
    }
    if(p.h_4) {
      ret = "sus4"
    }
    if(p.h_b3) {
      ret = "moll"
    }
    if(p.h_3) {
      ret = "dur"
    }
    return ret
  }
  
  func septErmitteln() -> String {
    var ret: String = ""
    if(p.h_6) {
      ret = "6"
    }
    if(p.h_7) {
      ret = "7"
    }
    if(p.h_b7) {
      ret = "b7"
    }
    return ret
  }
  
}
